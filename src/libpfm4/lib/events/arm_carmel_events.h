/*
 * Copyright (c) 2021 Barcelona Supercomputing Center (BSC). All rights reserved
 * Contributed by Alvaro Jover-Alvarez <alvaro.jover@bsc.es>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * NVIDIA Carmel Core - ARM v8.2-A
 * Based on "Cortex A57 Technical Reference Manual" - WIP
 */

static const arm_entry_t arm_carmel_pe[]={
	{.name = "SW_INCR",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x000,
	 .desc = "Instruction architecturally executed (condition check pass) Software increment"
	},
	{.name = "L1I_CACHE_REFILL",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x001,
	 .desc = "Level 1 instruction cache refill"
	},
	{.name = "L1I_TLB_REFILL",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x002,
	 .desc = "Level 1 instruction TLB refill"
	},
	{.name = "L1D_CACHE_REFILL",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x003,
	 .desc = "Level 1 data cache refill"
	},
	{.name = "L1D_CACHE",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x004,
	 .desc = "Level 1 data cache access"
	},
	{.name = "L1D_TLB_REFILL",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x005,
	 .desc = "Level 1 data TLB refill"
	},
	{.name = "INST_RETIRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x008,
	 .desc = "Instruction architecturally executed"
	},
	{.name = "EXC_TAKEN",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x009,
	 .desc = "Exception taken"
	},
	{.name = "EXC_RETURN",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x00a,
	 .desc = "Instruction architecturally executed (condition check pass) Exception return"
	},
	{.name = "CID_WRITE_RETIRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x00b,
	 .desc = "Instruction architecturally executed (condition check pass)  Write to CONTEXTIDR"
	},
	{.name = "BR_MIS_PRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x010,
	 .desc = "Mispredicted or not predicted branch speculatively executed"
	},
	{.name = "CPU_CYCLES",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x011,
	 .desc = "Cycles"
	},
	{.name = "BR_PRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x012,
	 .desc = "Predictable branch speculatively executed"
	},
	{.name = "MEM_ACCESS",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x013,
	 .desc = "Data memory access"
	},
	{.name = "L1I_CACHE",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x014,
	 .desc = "Level 1 instruction cache access"
	},
	{.name = "L1D_CACHE_WB",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x015,
	 .desc = "Level 1 data cache WriteBack"
	},
	{.name = "MEMORY_ERROR",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x01a,
	 .desc = "Memory error"
	},
	{.name = "TTBR_WRITE_RETIRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x01c,
	 .desc = "Instruction architecturally executed (condition check pass)  Write to translation table base"
	},
	{.name = "BR_RETIRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x021,
	 .desc = "Predictable branch speculatively retired"
	},
	{.name = "BR_MIS_PRED_RETIRED",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x022,
	 .desc = "Mispredicted or not predicted branch speculatively retired"
	},
	{.name = "STALL_FRONTEND",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x023,
	 .desc = "Cycle on which no operation issued because there are no operations to issue"
	},
	{.name = "STALL_BACKEND",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x024,
	 .desc = "Cycle on which no operation issued due to back-end resources being unavailable"
	},

	/* END Carmel specific events */
};

static const arm_entry_t arm_carmel_unc_pe[]={
	{.name = "L2D",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x016,
	 .desc = "Level 2 data cache access"
	},
	{.name = "L2D_REFILL",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x017,
	 .desc = "Level 2 data cache refill"
	},
	{.name = "L2D_CACHE_WB",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x018,
	 .desc = "Level 2 data cache WriteBack"
	},
	{.name = "BUS_ACCESS",
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x019,
	 .desc = "Bus access"
	},
	{.name = "BUS_CYCLES", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x01d,
	 .desc = "Bus cycles"
	},
	{.name = "L3D_CACHE_ALLOCATE", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x029,
	 .desc = "Level 3 unified cache allocation without refill counter. Counts every full cache line write into the L3 cache which does not cause a linefill"
	},
	{.name = "L3D_CACHE_REFILL", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x02a,
	 .desc = "Level 3 data cache refill"
	},
	{.name = "L3D_CACHE", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x02b,
	 .desc = "Level 3 data cache access"
	},
	{.name = "L3D_CACHE_WB", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x02c,
	 .desc = "Level 3 data cache WriteBack"
	},
	{.name = "L2D_LD", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x050,
	 .desc = "Level 2 data cache read access"
	},
	{.name = "L2D_ST", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x051,
	 .desc = "Level 2 data cache write access"
	},
	{.name = "L2D_REFILL_LD", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x052,
	 .desc = "Level 2 data cache read refill"
	},
	{.name = "L2D_REFILL_ST", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x053,
	 .desc = "Level 2 data cache write refill"
	},
	{.name = "L2D_REFILL_VICTIM", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x056,
	 .desc = "Level 2 data cache refill victim"
	},
	{.name = "L2D_PREFETCH_C0", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x0c5,
	 .desc = "Level 2 data cache C0 prefetches"
	},
	{.name = "L2D_PREFETCH_C1", // Carmel
	 .modmsk = ARMV8_ATTRS,
	 .code = 0x0c6,
	 .desc = "Level 2 data cache C1 prefetches"
	},
	/* END Carmel specific events */
};


int
pfm_carmel_unc_get_event_encoding(void *this, pfmlib_event_desc_t *e);
int
pfm_carmel_unc_get_perf_encoding(void *this, pfmlib_event_desc_t *e);